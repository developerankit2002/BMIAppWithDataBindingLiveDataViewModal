package com.example.bmiapp.modaldataclass

data class BmiUserData(
    private var name : String,
    private var MBN : String?,
    private var height_value : Double?,
    private var weight_value : Double?
)
