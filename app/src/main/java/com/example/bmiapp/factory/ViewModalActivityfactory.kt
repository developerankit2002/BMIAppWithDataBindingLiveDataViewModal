package com.example.bmiapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bmiapp.viewModal.ViewModalActivityviewModal

@Suppress("UNCHECKED_CAST")
class ViewModalActivityfactory(private val height_factory:Int, private val weight_factory:Int):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ViewModalActivityviewModal::class.java)){
            return ViewModalActivityviewModal(height_factory, weight_factory) as T
        }
       throw IllegalArgumentException(" Known Class ")
    }
}