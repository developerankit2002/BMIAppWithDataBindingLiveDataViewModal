package com.example.bmiapp.activity

import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.bmiapp.R
import com.example.bmiapp.databinding.ActivityMainBinding
import com.example.bmiapp.databinding.LayoutRateUsActivityBinding
import com.example.bmiapp.factory.ViewModalActivityfactory
import com.example.bmiapp.obserable.FieldObserable
import com.example.bmiapp.viewModal.ViewModalActivityviewModal

class MainActivity : AppCompatActivity() {
    //----------------------------------------------------------------------------------------------
    private lateinit var binding : ActivityMainBinding

    //----------------------------------------------------------------------------------------------
    private lateinit var factory : ViewModalActivityfactory

    //----------------------------------------------------------------------------------------------
    private lateinit var viewModal : ViewModalActivityviewModal

    //----------------------------------------------------------------------------------------------
    private lateinit var fieldobserver : FieldObserable


    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        //------------------------------------------------------------------------------------------
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //------------------------------------------------------------------------------------------
        factory = ViewModalActivityfactory(0, 0)
        //------------------------------------------------------------------------------------------
        viewModal = ViewModelProvider(this, factory)[ViewModalActivityviewModal::class.java]
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------
        fieldobserver = FieldObserable()
        binding.myViewData = viewModal
        //------------------------------------------------------------------------------------------
        binding.lifecycleOwner = this
        //------------------------------------------------------------------------------------------
        viewModal.res.observe(this) {
            binding.tvBmiValue.text = it.toString()
        }

        viewModal.errorMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }



    }

    // Here I Use MenuOption Some important Code
    //---------------------------------------------------------------------------------------------------------
    override fun onCreateOptionsMenu(menu : Menu?) : Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.manubars, menu)
        return super.onCreateOptionsMenu(menu)
    }
    //------------------------------------------------------------------------------------------------------------

    // I Use Option Menu Which make my Application Good and excellent
    //--------------------------------------------------------------------------------------------------------------
    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        when (item.itemId) {

            R.id.save_data -> {
                viewModal.saveData(this@MainActivity)
            }

            R.id.sub_option_dail -> {
                val intent = Intent(Intent.ACTION_CALL)
                intent.data = Uri.parse("tel:9625812224")
                startActivity(intent)
            }

            R.id.sub_option_email -> {
                val intent = Intent(Intent.ACTION_SENDTO)
                intent.data = Uri.parse("mailto:")
                intent.putExtra(Intent.EXTRA_EMAIL, "ankitbabu647@gmail.com")
                intent.putExtra(Intent.EXTRA_SUBJECT, " Leave to Attend Friend Wedding ")
                intent.putExtra(Intent.EXTRA_TEXT, " ")
                startActivity(intent)
            }

            R.id.sub_option_online_knowladge -> {
                val intent = Intent(this@MainActivity, WevViewForActivity::class.java)
                startActivity(intent)

            }


            R.id.sub_option_see_video_about_bmi -> {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://www.youtube.com/watch?v=oQhHW_hUAFg")
                )
                startActivity(intent)
            }


            R.id.about_app -> {
                val intent = Intent(this, AboutDeveloper::class.java)
                startActivity(intent)
            }


            R.id.bmichart -> {
                val intent = Intent(this, BMIActivity::class.java)
                startActivity(intent)
            }


            R.id.rate_us -> {
                val layoutRateUsActivityBinding =
                    LayoutRateUsActivityBinding.inflate(layoutInflater)
                showRatingBar(layoutRateUsActivityBinding)


            }

        }

        return super.onOptionsItemSelected(item)
    }
    //-------------------------------------------------------------------------------------------------------------------

    private fun showRatingBar(layoutRateUsActivityBinding : LayoutRateUsActivityBinding) {
        val dialog = Dialog(this)
        dialog.setContentView(layoutRateUsActivityBinding.root)
        dialog.setCancelable(false)
        val windowManager = WindowManager.LayoutParams()
        windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
        windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = windowManager
        dialog.show()
        layoutRateUsActivityBinding.ratingbar.setOnRatingBarChangeListener { ratingBar, _, _ ->
            layoutRateUsActivityBinding.btnSubmit.setOnClickListener {
                Toast.makeText(this, "Submit Successfully ${ratingBar.rating}", Toast.LENGTH_SHORT)
                    .show()
                dialog.dismiss()
            }
        }

    }


    // Here I Use Code for disable back button Action
    //----------------------------------------------------------------------------------------------
    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        showAlertDialog()
    }

    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage(" Do You Want to Exit ")
        alertDialog.setPositiveButton(
            "Yes"
        ) { _, _ -> finish() }

        alertDialog.setNegativeButton(
            "No"
        ) { _, _ -> }

        val alert : AlertDialog = alertDialog.create()
        alert.show()
    }

}