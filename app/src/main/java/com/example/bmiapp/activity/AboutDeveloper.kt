package com.example.bmiapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bmiapp.R

class AboutDeveloper : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_developer)
    }
}