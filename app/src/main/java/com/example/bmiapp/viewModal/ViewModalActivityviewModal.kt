package com.example.bmiapp.viewModal

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bmiapp.modaldataclass.BmiUserData
import com.example.bmiapp.obserable.FieldObserable
import kotlin.math.pow
import kotlin.math.round

class ViewModalActivityviewModal(weight_default_value : Int, height_default_value : Int):ViewModel() {
    private var height : MutableLiveData<Double> = MutableLiveData()
    private var weight : MutableLiveData<Double> = MutableLiveData()
    private var messageForUser : MutableLiveData<String> = MutableLiveData()
    var errorMessage : MutableLiveData<String> = MutableLiveData()
    private var usernamedata : MutableLiveData<String> = MutableLiveData()
    private var phoneNumber : MutableLiveData<String> = MutableLiveData()


    private var bmires : Double = 0.0
    private var isClear : Boolean = true


    var baseObserable : FieldObserable = FieldObserable()
    private lateinit var bmiUserData : BmiUserData


    // private lateinit var userdata:BmiUserData
    private var bmi : MutableLiveData<Double> = MutableLiveData()
    var res : LiveData<Double> = bmi
    var msg : LiveData<String> = messageForUser


    init {
        height.value = weight_default_value.toDouble()
        weight.value = height_default_value.toDouble()
        messageForUser.value = " Health Status "

    }


    fun validation() = if (isClear) {
//----------------------------------------------------Start isClear if condition --------------------------------------------------------------------------------------
        if (baseObserable.username.isNotEmpty() && baseObserable.phone.isNotEmpty() &&
            baseObserable.height.isNotEmpty() && baseObserable.weight.isNotEmpty()
        ) {
            //------------------------------- Start All Fields is Not Empty if Block -------------------------------------------------------------------------------------------------
            usernamedata.value = baseObserable.username
            if (usernamedata.value.toString() == "") {
                errorMessage.value = " Please Enter Valid User Name "
            }
            //--------------------------------------------------------------------------------------------------------------------------------
            phoneNumber.value = baseObserable.phone
            if (phoneNumber.value.toString().length != 10) {
                errorMessage.value = "Please Enter 10 Digits Phone Number "
            }

            if (usernamedata.value.toString().length > 30) {
                errorMessage.value = " You Have Cross Your Limit "

            }
            //--------------------------------------------------------------------------------------------------------------------------------
            height.value = baseObserable.height.toDouble()
            if (height.value.toString().toDouble() == 0.0) {
                errorMessage.value = " Please Enter Valid Height "
            }

            //--------------------------------------------------------------------------------------------------------------------------------
            weight.value = baseObserable.weight.toDouble()
            if (weight.value.toString().toDouble() == 0.0) {
                errorMessage.value = " Please Enter Valid Weight  "
            }

            // Here I Calculate BMI Value After Check All Conditions
            //------------------------------------------- Calculate BMI Value -------------------------------------------------------------------------------------
            val heightInMeter = height.value!! / 100.0
            bmires = weight.value!! / heightInMeter.pow(2)
            val format = round(bmires)
            bmi.value = format


            // this is Switch case to check Multiples values
            //--------------------------------------------------- Check All Possible Conditions -----------------------------------------------------------------------------
            when (bmi.value!!.toDouble()) {

                in 10.00..16.00 -> {
                    messageForUser.value = " You are Severely Underweight "
                }
                in 16.00..18.40 -> {
                    messageForUser.value = " You are Underweight "
                }
                in 18.50..24.90 -> {
                    messageForUser.value = " You are Normal "
                }
                in 25.00..29.90 -> {
                    messageForUser.value = " You are Overweight "
                }
                in 30.00..34.90 -> {
                    messageForUser.value = " You are Moderately Obese "
                }
                in 35.00..39.90 -> {
                    messageForUser.value = " You are Severely Obese "
                }
                in 40.00..50.00 -> {
                    messageForUser.value = " You are Morbidly Obese "
                }
                else -> {

                }

            }
            //------------------------------------End of When Block --------------------------------------------------------------------------------------------
        }
        //------------------------------------------Start if of else Block if is Inside in If --------------------------------------------------------------------------------------
        else {
            if (baseObserable.username.isEmpty() && baseObserable.phone.isEmpty() &&
                baseObserable.height.isEmpty() && baseObserable.weight.isEmpty()
            ) {
                errorMessage.value = "Please Fill Both are Fields"
            } else if (baseObserable.username.isEmpty()) {
                errorMessage.value = "Please Enter User Name "
            } else if (baseObserable.phone.isEmpty()) {
                errorMessage.value = " Please Enter Phone Number "
            } else if (baseObserable.height.isEmpty()) {
                errorMessage.value = "Please Enter Height "

            } else if (baseObserable.weight.isEmpty()) {
                errorMessage.value = "Please Enter Weight "
            }

        }

        isClear = false
        errorMessage.value = " Please Reset All Fields "


        //------------------------------------------End else Block else is Inside in If ------------------------------------------------------------
    }
    //------------------------------------------Start else of isclear block ------------------------------------------------------------
    else {
        baseObserable.username = ""
        baseObserable.phone = ""
        baseObserable.height = ""
        baseObserable.weight = ""
        messageForUser.value = ""
        bmi.value = 0.0

        isClear = true
        errorMessage.value = " Please Calculate BMI  "
    }


    fun saveData(context : Context) {
        if (baseObserable.username.isEmpty() && baseObserable.height.isEmpty() && baseObserable.weight.isEmpty()) {
            errorMessage.value = " Please Fill All Fields Very Carefully "
        } else if (baseObserable.username.isEmpty()) {
            errorMessage.value = " Please Fill User Name Field "
        } else if (baseObserable.phone.isEmpty()) {
            errorMessage.value = " Please Fill Phone Number Field "
        } else if (baseObserable.height.isEmpty()) {
            errorMessage.value = " Please Fill Height Field "
        } else if (baseObserable.weight.isEmpty()) {
            errorMessage.value = " Please Fill Weight Field "
        } else {
           showConfirmationDialog(context)
        }

//----------------------------------------------------------------------------------------
    }


    private fun showConfirmationDialog(context : Context) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage(" Do You Want to Save This Data ")
        alertDialog.setPositiveButton(
            "Yes"
        ) { _, _ ->
            val namevalue : String = usernamedata.value.toString()
            val mobileNumber : String? = phoneNumber.value?.toString()
            val heightvalue : Double? = height.value?.toDouble()
            val weightvalue : Double? = weight.value?.toDouble()

            bmiUserData = BmiUserData(
                name = namevalue,
                MBN = mobileNumber,
                height_value = heightvalue,
                weight_value = weightvalue
            )
            errorMessage.value = " Data Save Successfully "
        }

        alertDialog.setNegativeButton(
            "No"
        ) { _, _ -> }

        val alert : AlertDialog = alertDialog.create()
        alert.show()
    }
}